import tkinter as tk
import urllib.request
import urllib.error
import json
import sys
import threading

from tkinter import ttk
from tkinter import (font as tkfont)

from solution.client.step1 import Step1
from solution.client.step2 import Step2
from solution.client.step3 import Step3
from solution.defaults import *


class Loader(threading.Thread):

    def __init__(self):
        self.result = None
        super(Loader, self).__init__()

    def run(self):
        try:
            urlData = INDEX + "keys"
            webURL = urllib.request.urlopen(urlData)
            data = webURL.read()
            encoding = webURL.info().get_content_charset('utf-8')
            data = json.loads(data.decode(encoding))
            self.result = [True, data]
        except urllib.error.URLError:
            self.result = [False, None]


class Wizard(tk.Frame):
    def __init__(self, parent):
        super().__init__(parent)
        self.FILE = None
        self.BLIND_FILE = None
        self.HASH_FUNCTION = None
        self.INVERSE_FACTOR = None
        self.SIGNATURE = None
        self.BLIND_SIGNATURE = None

        self.OPERATION = {}

        self.SETTINGS = parent.SETTINGS

        self.current_step = None
        self.steps = [Step1(self), Step2(self), Step3(self)]

        self.button_frame = tk.Frame(self, bd=1, relief="raised")
        self.content_frame = tk.Frame(self)

        self.back_button = ttk.Button(self.button_frame, text="<< Back", command=self.back)
        self.next_button = ttk.Button(self.button_frame, text="Next >>", command=self.next)
        self.finish_button = ttk.Button(self.button_frame, text="Finish", command=self.finish)

        self.button_frame.pack(side="bottom", fill="x")
        self.content_frame.pack(side="top", fill="both", expand=True)

        self.show_step(0)

    def show_step(self, step):

        if self.current_step is not None:
            # remove current step
            current_step = self.steps[self.current_step]
            current_step.pack_forget()

        self.current_step = step

        new_step = self.steps[step]
        new_step.pack(fill="both", expand=True)
        new_step.update_data()

        if step == 0:
            # first step
            self.back_button.pack_forget()
            self.next_button.pack(side="right")
            self.finish_button.pack_forget()

        elif step == len(self.steps)-1:
            # last step
            self.back_button.pack(side="left")
            self.next_button.pack_forget()
            self.finish_button.pack(side="right")

        else:
            # all other steps
            self.back_button.pack(side="left")
            self.next_button.pack(side="right")
            self.finish_button.pack_forget()

    def next(self):
        self.show_step(self.current_step + 1)

    def back(self):
        self.show_step(self.current_step - 1)

    def finish(self):

        """ Finish App
        """
        sys.exit()


class MainApp(tk.Tk):

    def __init__(self, *args, **kwargs):

        tk.Tk.__init__(self, *args, **kwargs)
        self.title_font = tkfont.Font(family='Helvetica', size=18, weight="bold", slant="italic")
        tk.Tk.wm_title(self, "Blind signatures v1.0")
        self.SETTINGS = {}

        """ Always open window in the middle.
            To achieve this:
                1. determine screen resolution
                2. set default window size
                3. calculate window position as 
                
                X = (RESOLUTION_X / 2) - (APP_WINDOW_X / 2)
                Y = (RESOLUTION_Y / 2) - (APP_WINDOW_Y / 2) 
        """
        w = kwargs.get('w', 800)  # width for the Tk root    ,500
        h = kwargs.get('h', 500)  # height for the Tk root   ,400

        # get screen width and height
        ws = self.winfo_screenwidth()  # width of the screen
        hs = self.winfo_screenheight()  # height of the screen

        # calculate x and y coordinates for the Tk root window
        x = (ws / 2) - (w / 2)
        y = (hs / 2) - (h / 2)

        # set the dimensions of the screen
        # and where it is placed
        self.geometry('%dx%d+%d+%d' % (w, h, x, y))
        self.minsize(w, h)

        """ Create Start page.
            Create new Frame as Container
            - add items to it
            - add Start buttons
            AFTERT START SCHEME:
                - delete container
                - start wizard
        """
        self.container = tk.Frame(self)
        self.container.pack(side="top", fill="both", expand=True)
        self.container.grid_rowconfigure(0, weight=1)
        self.container.grid_columnconfigure(0, weight=1)

        self.header = tk.Label(self.container, text="Blind signatures client", font=("Helvetica", 24))
        self.header.pack()

        self.description = tk.Label(
            self.container,
            text="This is simple client for blind signatures protocols. "
                 "To continue please select, which protocol you woud like to use",
            font=("Arial", 12),
            wraplength=350
        )
        self.description.pack()

        self.start_button = tk.Button(
            self.container, text="Start RSA Protocol", command=self.start
        )
        self.start_button.pack(pady=20)
        self.refresh_button = tk.Button(
            self.container, text="Load Keys", command=self.load_keys
        )
        self.refresh_button.pack(pady=20)

        self.error_label = tk.Label(
            self.container,
            text="",
            font=("Arial", 13),
            wraplength=350
        )
        self.error_label.pack()

        self.load_keys()

    def start(self):
        self.container.destroy()
        Wizard(self).pack(side="top", fill="both", expand=True)

    def hide_error(self):
        self.error_label.configure(text='')

    def load_keys(self):

        loader = Loader()
        self.error_label.config(fg="black")
        self.error_label.configure(text='Attempt to load keys...')
        loader.start()
        loader.join()
        [ans, data] = loader.result
        if ans:
            self.SETTINGS = {'keys': data}
            self.error_label.configure(text='Keys were downloaded successfully')
            self.start_button.config(state="normal")
        else:
            self.SETTINGS = {'keys': None}
            self.error_label.config(fg="red")
            self.error_label.configure(text='Can not contact server. Try again later.')
            self.start_button.config(state="disabled")


def start_app():
    app = MainApp()
    app.mainloop()


if __name__ == "__main__":
    start_app()
