import tkinter as tk
from tkinter import ttk
import json
import requests
import base64
from solution.utils import write_file


class Step2(tk.Frame):
    def __init__(self, parent, **kwargs):
        super().__init__(parent)

        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)

        self.master = parent

        self.row = 0
        self.header = tk.Label(self, text="Protocol with RSA PSS signature", bd=2, relief="groove", font=("Helvetica", 16))
        # header.pack(side="top", fill="x") #  # side="top"
        self.header.grid(row=self.row, columnspan=3, sticky=tk.N + tk.E + tk.W, padx=5, pady=5)
        self.row += 1

        self.subhead = tk.Label(self, text="Step two - Sign blinded message", font=("Helvetica", 14))
        self.subhead.grid(row=self.row, columnspan=3, sticky=tk.N + tk.S + tk.E + tk.W, padx=5, pady=5)
        self.row += 1

        self.current_label = tk.Label(self, text="Current settings", font=("Helvetica", 12))
        self.current_label.grid(row=self.row, columnspan=1, sticky=tk.W, padx=5, pady=5)
        self.row += 1

        self.textarea = tk.Text(self, height=10, width=100)
        self.textarea.grid(row=self.row, columnspan=3, sticky=tk.N + tk.S + tk.E + tk.W, padx=5, pady=5)
        self.textarea.insert(tk.END, "Testovacia sprava")
        self.row += 1

        self.actions_label = tk.Label(self, text="Actions", font=("Helvetica", 12))
        self.actions_label.grid(row=self.row, sticky=tk.W, padx=5, pady=5)
        self.row += 1

        self.send_button = ttk.Button(self, text="Send for signing", command=self.send_file)
        # self.file_button.pack(side="left") # .grid(row=1, column=0)
        self.send_button.grid(row=self.row, column=0)
        self.row += 1

        self.error_label = tk.Label(self, text="", font=("Helvetica 12 bold"), height=3)
        self.error_label.grid(row=self.row, column=0)

    def _post_request(self):
        try:
            if self.master.OPERATION is not None and len(self.master.OPERATION) > 0:
                blind_bytes = self.master.OPERATION.get('blind_message')
                b64_bytes = base64.b64encode(blind_bytes)
                base64_string = b64_bytes.decode('utf-8')
                data = {
                    'name': self.master.OPERATION.get('public_key_name'),
                    'data': base64_string
                }
                url = 'https://localhost/sign'
                headers = {'content-type': 'application/json'}
                response = requests.post(url, data=json.dumps(data), headers=headers, verify=False)
                return response
        except requests.exceptions.ConnectionError:
            raise requests.exceptions.ConnectionError("Connection lost. Try again later.")

    def send_file(self):

        try:
            response = self._post_request()
            if response is None:
                self.error_label.configure(text='There is network problem with server. Try again later.')
                self.error_label.config(fg="red")
                return
            response_dict = json.loads(response.content)
            b64data = response_dict.get('data')

            signature = base64.decodebytes(b64data.encode())
            filepath = self.master.OPERATION.get('file_path') + ".blindsig.bin"
            write_file(filepath, signature)
            self.master.OPERATION['blind_signature_file'] = filepath
            self.master.OPERATION['blind_signature_data'] = signature
            self.error_label.configure(text="Blind signature from server received and saved successfully.")
            self.error_label.config(fg="black")
        except requests.exceptions.ConnectionError as e:
            self.error_label.configure(text=e)
            self.error_label.config(fg="red")


    def update_data(self):
        self.textarea.configure(state='normal')
        self.error_label.configure(text='')
        self.error_label.config(fg="black")
        if self.master.OPERATION is not None and len(self.master.OPERATION) > 0:
            self.send_button.config(state="normal")
            filename = self.master.OPERATION.get('filename')
            digest = self.master.OPERATION.get('digest').get()
            blind_message_file = self.master.OPERATION.get('blind_message_file')
            inverse_file = self.master.OPERATION.get('inverse_file')
            public_key_name = self.master.OPERATION.get('public_key_name')
            hash_value = self.master.OPERATION.get('hash_value')
            message = 'Filename: {0}\nHash Function: {1}\nBlind File: {2}\n' \
                      'Inverse Factor: {3}\nPublic key name: {4}\n\n'\
                      'File digest value (hex): {5}'\
                .format(filename, digest, blind_message_file, inverse_file, public_key_name, hash_value)

        # self.text.delete(tk.FIRST, tk.END)
        else:
            message = "No previous results"
            self.error_label.configure(text='Can not send no data. Go back to step one and create blind file')
            self.error_label.config(fg="red")
            self.send_button.config(state="disabled")

        self.textarea.delete(1.0, tk.END)
        self.textarea.insert(tk.END, message)
        self.textarea.configure(state='disabled')
