import tkinter as tk
import base64
import queue

from tkinter import ttk
from PyBlindSchemes.Blindsignature.rsa_pss import PSS_BlindSigScheme
from Crypto.PublicKey import RSA
from solution.utils import (write_file)
from solution.client.async_action import AsyncActionFile


class Step3(tk.Frame):
    def __init__(self, parent, **kwargs):
        super().__init__(parent)

        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(0, weight=1)

        self.queue = queue.Queue()

        self.key_name = tk.StringVar()

        self.master = parent
        self.columnspan = 2
        self.row = 0
        self.header = tk.Label(self, text="Protocol with RSA PSS signature", bd=2, relief="groove",
                               font=("Helvetica", 16))
        # header.pack(side="top", fill="x") #  # side="top"
        self.header.grid(row=self.row, columnspan=self.columnspan, sticky=tk.N + tk.E + tk.W, padx=5, pady=5)
        self.row += 1

        self.subhead = tk.Label(self, text="Step three - Unblind & Verify message", font=("Helvetica", 14))
        self.subhead.grid(row=self.row, columnspan=self.columnspan, sticky=tk.N + tk.S + tk.E + tk.W, padx=5, pady=5)
        self.row += 1

        self.current_label = tk.Label(self, text="Current settings", font=("Helvetica", 12))
        self.current_label.grid(row=self.row, columnspan=self.columnspan, sticky=tk.W, padx=5, pady=5)
        self.row += 1

        self.textarea = tk.Text(self, height=10, width=100)
        self.textarea.grid(row=self.row, columnspan=self.columnspan, sticky=tk.N + tk.S + tk.E + tk.W, padx=5, pady=5)

        self.row += 1
        self.progress_label = tk.Label(self, anchor=tk.W, text="Progress", font=("Helvetica", 12))
        self.progress_label.grid(row=self.row, columnspan=self.columnspan, sticky=tk.W, padx=5, pady=5)
        self.row += 1
        self.progressbar = ttk.Progressbar(self, orient="horizontal", length=300)
        self.progressbar.grid(row=self.row, columnspan=self.columnspan,
                              sticky=tk.N + tk.S + tk.E + tk.W, padx=5, pady=5)
        # self.progressbar.grid_remove()
        self.row += 1

        self.actions_label = tk.Label(self, text="Actions", font=("Helvetica", 12))
        self.actions_label.grid(row=self.row, sticky=tk.W, padx=5, pady=5)

        self.row += 1
        self.send_button = ttk.Button(self, text="Unblind message", command=self.unblind_file)
        # self.file_button.pack(side="left") # .grid(row=1, column=0)
        self.send_button.grid(row=self.row, column=0)
        # self.row += 1

        self.verify_button = ttk.Button(self, text="Verify message", command=self.verify_action, state='disabled')
        # self.file_button.pack(side="left") # .grid(row=1, column=0)
        self.verify_button.grid(row=self.row, column=1)
        self.row += 1

        self.error_label = tk.Label(self, text="", font=("Helvetica 12 bold"), height=3)
        self.error_label.grid(row=self.row, column=0, columnspan=self.columnspan)

    def update_data(self):
        self.textarea.configure(state='normal')
        self.error_label.configure(text='')
        self.error_label.config(fg="black")
        if self.master.OPERATION is not None and len(self.master.OPERATION) > 0:
            self.send_button.config(state="normal")
            filename = self.master.OPERATION.get('filename')
            digest = self.master.OPERATION.get('digest').get()
            blind_message_file = self.master.OPERATION.get('blind_message_file')
            inverse_file = self.master.OPERATION.get('inverse_file')
            public_key_name = self.master.OPERATION.get('public_key_name')
            blinded_message = self.master.OPERATION.get('blind_signature_file')
            hash_value = self.master.OPERATION.get('hash_value')
            message = 'Filename: {0}\nHash Function: {1}\nBlind File: {2}\n' \
                      'Inverse Factor: {3}\nPublic key name: {4}\n\n' \
                      'File digest value (hex): {5}\n\n' \
                      'Blinded message: {6}' \
                .format(filename, digest, blind_message_file, inverse_file,
                        public_key_name, hash_value, blinded_message)

            if blinded_message is None:
                self.send_button.config(state="disabled")
                self.error_label.configure(
                    text='Can not continue without blind signature. Go back to step two and create blind signature'
                )
        else:
            message = "No previous results"
            self.error_label.configure(
                text='Can not continue with no data. Go back to step two and create blind signature'
            )
            self.error_label.config(fg="red")
            self.send_button.config(state="disabled")

        self.textarea.delete(1.0, tk.END)
        self.textarea.insert(tk.END, message)
        self.textarea.configure(state='disabled')

    def get_key_from_name(self):
        key_name = self.master.OPERATION.get('public_key_name')
        key_data = self.master.SETTINGS['keys'].get(key_name)
        key_bytes = base64.decodebytes(key_data.encode())
        key = RSA.import_key(key_bytes)
        return key

    def unblind_file(self):
        if self.master.OPERATION is not None and len(self.master.OPERATION) > 0:
            blind_signature = self.master.OPERATION.get('blind_signature_data')
            inverse = self.master.OPERATION.get('inverse')
            key = self.get_key_from_name()
            scheme = PSS_BlindSigScheme(key)

            signature = scheme.unblind_message(blind_signature, inverse)
            signature_file = self.master.OPERATION.get('file_path') + ".signature.bin"
            self.master.OPERATION['signature_data'] = signature
            self.master.OPERATION['signature_file'] = signature_file
            write_file(signature_file, signature)
            self.error_label.configure(text='Created valid signature and saved')
            self.error_label.config(fg="black")
            self.verify_button.config(state="normal")
        else:
            self.error_label.configure(
                text='Can not continue with no data. Go back to step two and create blind signature.')
            self.error_label.config(fg="red")
            self.send_button.config(state="disabled")

    def toggle_state(self, state):
        self.send_button.config(state=state)
        self.verify_button.config(state=state)

    def verify_action(self):
        self.key_name.set(self.master.OPERATION.get('public_key_name'))
        self.toggle_state(tk.DISABLED)
        thread = AsyncActionFile(self, 'verify')
        thread.daemon = True
        thread.start()
        self.poll_thread(thread)

    def poll_thread(self, thread):
        self.check_queue()
        if thread.is_alive():
            self.after(100, lambda: self.poll_thread(thread))
        else:
            self.toggle_state(tk.NORMAL)

    def check_queue(self):
        while self.queue.qsize():
            try:
                [ans, value] = self.queue.get(0)
                if ans == 'size_file':
                    self.progressbar['maximum'] = value
                if ans == 'step':
                    step = value
                    self.progressbar.step(step)
                if ans == 'result':
                    self.error_label.configure(text=value)
                    self.error_label.config(fg="black")
                    self.toggle_state(tk.NORMAL)
                if ans == 'error':
                    self.error_label.config(fg="red")
                    self.error_label.configure(text=value)
                    self.toggle_state(tk.NORMAL)
            except queue.Empty:
                pass
