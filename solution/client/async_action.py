import threading
import base64
import os
from Crypto.PublicKey import RSA
from Crypto.Util.number import (long_to_bytes)
from solution.utils import (create_hash_obj, write_file, read_file)
from PyBlindSchemes.Blindsignature.rsa_pss import PSS_BlindSigScheme


class AsyncActionFile(threading.Thread):
    def __init__(self, master, action):
        super().__init__()
        self.step_one = master
        self.action = action

    def _check_file_path(self):
        if self.step_one.master.FILE is None:
            raise ValueError("You have selected no file")

    def _check_key_name(self):
        if not self.step_one.key_name.get():
            raise ValueError("Missing public key. Please select one key.")

    def blind_file(self, scheme, mhash, path):
        [data, inverse] = scheme.blind_message(mhash)
        blind_file = path + ".bm"
        inverse_file = path + ".inv"
        inverse_bytes = long_to_bytes(inverse)
        b64inverse = base64.encodebytes(inverse_bytes)
        write_file(blind_file, data)
        write_file(inverse_file, b64inverse)
        result = {
            'blind_message': data,
            'inverse': inverse,
            'file_path': path,
            'inverse_file': inverse_file,
            'blind_message_file': blind_file,
            'hash_value': mhash.hexdigest(),
            'hash_object': mhash

        }
        self.step_one.queue.put(['result', result])

    def answer_verify(self, scheme, mhash, signature):
        try:
            scheme.verify_message(mhash, signature)
            return True
        except ValueError:
            return False

    def verify_file(self, scheme, mhash):
        if self.step_one.master.OPERATION is not None and len(self.step_one.master.OPERATION) > 0:
            signature = self.step_one.master.OPERATION.get('signature_data')
            # signature_file = self.step_one.master.OPERATION.get('signature_file')
            # signature = read_file(signature_file, 'rb')
            ans = self.answer_verify(scheme, mhash, signature)
            if ans:
                self.step_one.queue.put(['result', 'Verification OK'])
            else:
                self.step_one.queue.put(['error', 'Verification Failure'])

    def run(self):
        try:
            self._check_file_path()
            self._check_key_name()

            key_data = self.step_one.master.SETTINGS['keys'].get(self.step_one.key_name.get())
            path = os.path.abspath(self.step_one.master.FILE)
            key_bytes = base64.decodebytes(key_data.encode())
            hash_function = self.step_one.master.HASH_FUNCTION.get()
            mhash = create_hash_obj(hash_function, path, self.step_one.queue)
            key = RSA.import_key(key_bytes)
            scheme = PSS_BlindSigScheme(key)

            if self.action == 'blind':
                self.blind_file(scheme, mhash, path)

            if self.action == 'verify':
                self.verify_file(scheme, mhash)

        except ValueError as e:
            self.step_one.queue.put(['error', e])
