import tkinter as tk
from tkinter import ttk
from tkinter import (font as tkfont, filedialog)
import queue
from solution.client.async_action import AsyncActionFile


class Step1(tk.Frame):

    def __init__(self, parent, **kwargs):
        super().__init__(parent)

        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)
        self.master = parent

        self.queue = queue.Queue()

        self.key_name = tk.StringVar()
        self.master.HASH_FUNCTION = tk.StringVar()
        self.row = 0
        self.columnspan = 2
        self.border = 2
        self.relief = 'groove'


        self.header = tk.Label(self, text="Protocol with RSA PSS signature", bd=2,
                               relief="groove", font=("Helvetica", 16))

        self.header.grid(row=self.row, columnspan=self.columnspan, sticky=tk.N + tk.E + tk.W, padx=5, pady=5)
        self.row += 1

        self.sub_title = tk.Label(self, text="Step one - Blind message", font=("Helvetica", 14))
        self.sub_title.grid(row=self.row, sticky=tk.N, columnspan=self.columnspan)
        self.row += 1

        self.label_select_input = tk.Label(
            self, font=("Helvetica", 12), wraplength=350, text="Select input file", anchor=tk.W
        )
        self.label_select_input.grid(row=self.row, sticky=tk.W, columnspan=2, padx=5, pady=5)
        self.row += 1

        self.select_frame = tk.Frame(self)
        self.select_frame.grid(row=self.row, sticky=tk.W + tk.E, pady=10, columnspan=2)

        """
        self.select_frame.columnconfigure(0, weight=1)
        self.select_frame.columnconfigure(1, weight=2)
        """

        self.file_button = ttk.Button(self.select_frame, text="Select file", command=self.select_file)
        self.file_button.grid(row=0, column=0, padx=5, pady=5, sticky=tk.W)

        filename = self.master.FILE if self.master.FILE is not None else "Choose file ..."
        self.file_label = tk.Entry(self.select_frame, text=filename, font=("Helvetica", 10), width=100)
        self.file_label.grid(row=0, column=1, sticky=tk.W)
        self.file_label.insert(tk.END, filename)
        self.file_label.config(state=tk.DISABLED)

        self.row += 1
        self.label_available_keys = tk.Label(self, anchor=tk.W, text="Available public keys", font=("Helvetica", 12))
        self.label_available_keys.grid(row=self.row, sticky=tk.W, padx=5)

        self.row += 1

        self.key_frame = tk.Frame(self)
        self.key_frame.grid(row=self.row, sticky=tk.W + tk.E, pady=10, columnspan=2)

        self.select_keys = tk.Label(self.key_frame, anchor=tk.W, text="Select public key", font=("Helvetica", 10))
        self.select_keys.grid(row=0, sticky=tk.W, padx=5)

        filenames = list(self.master.SETTINGS['keys'].keys())
        # self.box_value = tk.StringVar()
        self.option_menu = ttk.Combobox(self.key_frame, textvariable=self.key_name, state="readonly")
        # self.option_menu.bind("<<ComboboxSelected>>", self.combo_select)
        self.option_menu['values'] = filenames
        self.option_menu.current(0)
        self.option_menu.grid(row=0, column=1, sticky=tk.W)
        self.row += 1

        self.label_hash_functions = tk.Label(self, text="Available hash funtions", anchor=tk.W, font=("Helvetica", 12))
        self.label_hash_functions.grid(row=self.row, sticky=tk.W, padx=5)
        self.row += 1

        self.master.HASH_FUNCTION.set('SHA256')
        hash_functions = [("SHA-2 224", 'SHA224'), ("SHA-2 256", 'SHA256'), ("SHA-2 384", 'SHA384'),
                          ("SHA-2 512", 'SHA512'), ("SHA-3 224", 'SHA3_224'), ("SHA-3 256", 'SHA3_256'),
                          ("SHA-3 384", 'SHA3_384'), ("SHA-3 512", 'SHA3_512')]

        self.hash_functions_frame = tk.Frame(self)
        self.hash_functions_frame.grid(row=self.row, sticky=tk.W, padx=10)

        i = 0
        self.buttons_hash_function = []
        for text, mode in hash_functions:
            b = ttk.Radiobutton(self.hash_functions_frame, text=text, variable=self.master.HASH_FUNCTION, value=mode)
            b.grid(row=int(i / 4), column=i % 4)
            self.buttons_hash_function.append(b)
            i += 1

        self.row += 1

        self.progress_label = tk.Label(self, anchor=tk.W, text="Progress", font=("Helvetica", 12))
        self.progress_label.grid(row=self.row, columnspan=self.columnspan, sticky=tk.W, padx=5, pady=5)

        self.row += 1
        self.progressbar = ttk.Progressbar(self, orient="horizontal", length=300)
        self.progressbar.grid(row=self.row, columnspan=self.columnspan,
                              sticky=tk.N + tk.S + tk.E + tk.W, padx=5, pady=5)
        self.row += 1

        self.label_blind_action = tk.Label(self, anchor=tk.W, text="Actions", font=("Helvetica", 12))
        self.label_blind_action.grid(row=self.row, column=0, padx=5, sticky=tk.W)
        self.row += 1

        self.action_button = ttk.Button(self, text="Create blind file", command=self.blind_action)
        self.action_button.grid(row=self.row, column=0, columnspan=self.columnspan)
        self.row += 1
        self.error = tk.Label(self, text="", font=("Helvetica 12 bold"), height=3)
        self.error.grid(row=self.row ,columnspan=self.columnspan)
        self.error.config(fg="red")

    def select_file(self):
        self.master.FILE = filedialog.askopenfilename(initialdir="/", title="Select file")
        self.file_label.config(state=tk.NORMAL)
        self.file_label.delete(0, tk.END)
        self.file_label.insert(tk.END, self.master.FILE)
        self.file_label.config(state=tk.DISABLED)
        self.error.configure(text='')

    def toggle_state(self, state):
        self.action_button.config(state=state)
        self.file_button.config(state=state)
        self.option_menu.config(state=state)
        for button in self.buttons_hash_function:
            button.config(state=state)

    def blind_action(self):
        self.toggle_state(tk.DISABLED)
        self.error.configure(text='')
        thread = AsyncActionFile(self, 'blind')
        thread.daemon = True
        thread.start()
        self.poll_thread(thread)

    def poll_thread(self, thread):
        self.check_queue()
        if thread.is_alive():
            self.after(100, lambda: self.poll_thread(thread))
        else:
            self.action_button.config(state=tk.NORMAL)

    def check_queue(self):
        while self.queue.qsize():
            try:
                [ans, value] = self.queue.get(0)
                if ans == 'size_file':
                    self.progressbar['maximum'] = value
                if ans == 'step':
                    step = value
                    self.progressbar.step(step)
                if ans == 'result':
                    self.master.OPERATION = {
                        'blind_message': value.pop('blind_message'),
                        'inverse': value.pop('inverse'),
                        'digest': self.master.HASH_FUNCTION,
                        'file_path': value.pop('file_path'),
                        'filename': self.master.FILE,
                        'inverse_file': value.pop('inverse_file'),
                        'blind_message_file': value.pop('blind_message_file'),
                        'public_key_name': self.key_name.get(),
                        'hash_value': value.pop('hash_value'),
                        'hash_object': value.pop('hash_object')
                    }
                    self.error.configure(text='Blindfile created. You can continue with next step.')
                    self.error.config(fg="black")
                    self.toggle_state(tk.NORMAL)
                if ans == 'error':
                    self.error.config(fg="red")
                    self.error.configure(text=value)
                    self.toggle_state(tk.NORMAL)
            except queue.Empty:
                pass

    def update_data(self):
        pass
