from flask import Flask, request
from PyBlindSchemes.Blindschemes.rsablindscheme import RSABlindScheme
from PyBlindSchemes.Blindsignature.rsa_pss import PSS_BlindSigScheme
from solution.defaults import *
from solution.utils import (base64string_to_bytes, bytes_to_base64string)

import base64
import json
import os


app = Flask(__name__)


MAPPING = {'public_key.pem': 'private_key.pem', 'elections.pem': 'private_elections.pem'}


def get_keys_dir():
    """ Get directory `keys`
    """

    parent_dir = os.path.abspath(os.path.join(__file__, "../../.."))
    return os.path.abspath(os.path.join(parent_dir, 'keys'))


def find_all_keys(encoding='utf-8'):

    key_dir = get_keys_dir()
    keys = {}
    for file in os.listdir(key_dir):
        current = os.path.join(key_dir, file)
        if os.path.isfile(current):
            rsa_key = RSABlindScheme.import_key(current)
            if rsa_key.has_private():
                continue
            else:
                with open(current, "rb") as f:
                    data = f.read()
                    keys[file] = bytes_to_base64string(data, encoding)
    return keys


def sign_rsa_pss(key_name, data):
    global MAPPING
    passphrase = PASSPHRASE
    private_key_name = MAPPING.get(key_name)
    key_dir = get_keys_dir()
    key_file = os.path.abspath(os.path.join(key_dir, private_key_name))
    scheme_key = PSS_BlindSigScheme.import_key(key_file, passphrase=passphrase)
    schema = PSS_BlindSigScheme(scheme_key)
    blind_signature = schema.sign_message(data)
    return blind_signature


@app.route('/keys', methods=['GET'])
def get_keys():
    content = find_all_keys()
    return get_json_response(content)


def get_json_response(data):
    response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )
    return response


@app.route('/sign', methods=['POST'])
def sign():
    try:
        if request.json:
            content = request.json
            key_name = content.get('name')
            b64data = content.get('data')
            data = base64.decodebytes(b64data.encode())
            blind_sig = sign_rsa_pss(key_name, data)
            base64_bytes = base64.b64encode(blind_sig)
            base64_string = base64_bytes.decode('utf-8')
            result = {"result": True, "data": base64_string}
            return get_json_response(result)

    except (ValueError, TypeError) as e:
        return get_json_response({"result": False, "answer": e})


if __name__ == '__main__':
    par_dir = os.path.abspath(os.path.join(__file__, "../../.."))
    cert = os.path.abspath(os.path.join(par_dir, 'config/localhost.crt'))
    key = os.path.abspath(os.path.join(par_dir, 'config/localhost.key'))

    context = (cert, key)
    app.run(port=PORT, ssl_context=context)
