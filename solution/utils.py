from Crypto.Hash import (SHA3_512, SHA3_384, SHA3_256, SHA3_224, SHA512, SHA384, SHA256, SHA224)
import base64
import os

def generate_file_hash_obj(mhash, filename, queue=None, block_size=2 ** 16):
    with open(filename, "rb") as f:
        m = mhash
        s = os.path.getsize(filename)
        queue.put(["size_file", s])
        while True:
            buf = f.read(block_size)
            if not buf:
                break
            if queue:
                queue.put(["step", len(buf)])
            m.update(buf)
        return m


def valid_hash_functions():
    return ['SHA512', 'SHA384', 'SHA256', 'SHA224', 'SHA3_512', 'SHA3_384', 'SHA3_256', 'SHA3_224']

def read_file(filename, mode=None):
    if mode is None:
        mode = "r"
    with open(filename, mode) as f:
        return f.read()

def write_file(filename, data, mode="wb"):
    with open(filename, mode) as f:
        f.write(data)


def get_hash_object(hash_type):
    if not isinstance(hash_type, str):
        raise TypeError("Argument is not a string")
    functions = valid_hash_functions()
    if hash_type.upper() not in functions:
        raise ValueError("Unsupported hash function")
    if hash_type == "SHA224":
        return SHA224.new()
    elif hash_type == "SHA256":
        return SHA256.new()
    elif hash_type == "SHA384":
        return SHA384.new()
    elif hash_type == "SHA512":
        return SHA512.new()
    elif hash_type == "SHA3_224":
        return SHA3_224.new()
    elif hash_type == "SHA3_256":
        return SHA3_256.new()
    elif hash_type == "SHA3_384":
        return SHA3_384.new()
    elif hash_type == "SHA3_512":
        return SHA3_512.new()
    else:
        raise ValueError("Unsupported hash function")


def create_hash_obj(hash_name, filename, queue=None):
    return generate_file_hash_obj(get_hash_object(hash_name), filename, queue)


def bytes_to_base64string(input_bytes, encoding='utf-8'):
    base64_bytes = base64.b64encode(input_bytes)
    base64_string = base64_bytes.decode(encoding)
    return base64_string


def base64string_to_bytes(base64string):
    return base64.decodebytes(base64string.encode())
