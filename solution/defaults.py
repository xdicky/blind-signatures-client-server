import os


""" SERVER SETTINGS
"""

DOMAIN = "https://localhost"
PORT = 443
INDEX = "{0}:{1}/".format(DOMAIN, PORT)

""" Maps keys  as KEY=VALUE
    
    PUBLIC KEY = PRIVATE KEY
            ----->
"""
MAPPED_KEYS = {'public_key.pem': 'private_key.pem'}

PASSPHRASE = None

""" 
    Certificate and key for HTTPS
"""


""" CLIENT SETTINGS
"""

